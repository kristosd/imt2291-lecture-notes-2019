<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Motta data fra forms</title>
  <style media="screen">
    label {
      display: inline-block;
      width: 85px;
      float: left;
    }

    input {
      display: inline-block;
      width; 150px;
    }
  </style>
</head>
<body>
  <h1>Form med method=post</h1>
  <form action="example1.php" method="post">
    <label for="name">Name</label><input type="text" name="name" id="name"><br>
    <label>Password<input type="text" name="pwd"></label>
    <input type="submit" name="submit" value="Log in">
  </form>
  <br clear="both">
  <h1>Form med method=get</h1>
  <form action="example1.php" method="get">
    <label for="searchTerm">Search for</label><input type="text" name="searchTerm" id="searchTerm"><br>
    <label for="modifier">Modifier</label><input type="text" name="modifier" id="modifier"><br>
    <input type="submit" name="search" value="search">
  </form>
  <?php
    if (isset($_POST['name'])) {
      echo ("<h1>Data sendt som POST data</h1>\n<pre>");
      print_r($_POST);
      echo ("</pre>");
      echo "<p>Navnet som er sendt er {$_POST['name']}</p>";
    }
    if (isset($_GET['searchTerm'])) {
      echo ("<h1>Data sendt som GET data</h1>\n<pre>");
      print_r($_GET);
      echo ("</pre>");
      echo "<p>Du søker etter {$_GET['searchTerm']}</p>";
    }
   ?>
</body>
</html>
