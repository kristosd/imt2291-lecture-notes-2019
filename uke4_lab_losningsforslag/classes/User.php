<?php
/**
 * User class handles all user related tasks. When created the constructor
 * will check the $_POST and $_SESSION and $_COOKIE variables to handle
 * login/logout and session management tasks.
 * This class is also used to add, remove, list, search and delete users.
 */
class User {
  private $uid = -1;
  private $userData = [];
  private $db;

  /**
   * Handles login/logout through the $_POST superglobal, handles session
   * managment trough the $_SESSION and $_COOKIE superglobals.
   *
   * @param PDO $db stores a reference to this PDO connection object,
   * used for all database interaction.
   */
  public function __construct($db) {
    $this->db = $db;

    if (isset($_POST['uname'])) {
      $this->login($_POST['uname'], $_POST['pwd']);
    } else if (isset($_POST['logout'])) {
      unset($_SESSION['uid']);
    } else if (isset($_SESSION['uid'])) {
      $this->uid = $_SESSION['uid'];
    }
  }

  public function loggedIn() {
    return $this->uid>-1;
  }

  /**
   * Utility function used to log in a user. Sets the $_SESSION['uid'] to the id
   * of the user if a user exists with the given username/password.
   *
   * @param  String $uname the username for the user that attempts to log in
   * @param  String $pwd   the password for the user that attempts to log in
   * @return Array        element status='OK' on success, status='FAIL' on failure.
   *                      If login failed, errorMessage contains a message indicating
   *                      if it was a bad username or a bad password.
   */
  public function login($uname, $pwd) {
    $sql = 'SELECT id, email, password, givenName, familyName, department, userType FROM user WHERE email=?';
    $sth = $this->db->prepare ($sql);
    $sth->execute (array($uname));
    if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
      if (password_verify($pwd, $row['password'])) {
        $this->userData = $row;
        $_SESSION['uid'] = $row['id'];
        $this->uid = $row['id'];
        return array('status'=>'OK');
      } else {
        return array('status'=>'FAIL', 'errorMessage'=>'Bad password');
      }
    } else {
      return array('status'=>'FAIL', 'errorMessage'=>'No such user');
    }
  }

  /**
   * Adds a user to the database, all information about the user to created
   * should be in the $userData array.
   * Returns an array with status and inforamtion about the user that was added.
   *
   * @param Array $userData must include the elements :
   *              uname: the username(email) for the user to create
   *              pwd: the original pwd given by the user, will be hashed
   *              givenName: the given name (first name) of the user
   *              familyName: the family name (last name) of the user
   *              department: optional, will be set if given
   *              userType: 'student', 'teacher', 'admin', default is 'student'
   * @return Array with status=OK and id=id of the newly created user on success,
   *              if no user can be created status will be set to FAIL and
   *              errorMessage wihh contain an error message describing the error.
   */
  public function addUser($userData) {
    $sql = 'INSERT INTO user (email, password, givenName, familyName';
    $extra = '';

    // Optional fields
    if (isset($data['department'])) {
      $sql .= ', department';
      $extra .= ', ';
    }
    if (isset($data['userType'])) {
      $sql .= ', userType';
      $extra = ',? ';
    }
    $sql .= ") VALUES (?, ?, ?, ?$extra)";
    $sth = $this->db->prepare($sql);

    // Create data array to send to database
    $sqlData = array ($userData['uname'], password_hash($userData['pwd'], PASSWORD_DEFAULT),
                      $userData['givenName'], $userData['familyName']);
    if (isset($userData['department'])) {
      array_push ($sqlData, $userData['department']);
    }
    if (isset($userData['userType'])) {
      array_push($sqlData, $userData['userType']);
    }

    // Send query to database
    $sth->execute ($sqlData);

    // Query should create one new row
    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['id'] = $this->db->lastInsertId();
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Username already taken';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Deletes the user with the given ID from the database.
   *
   * @param  Number $id the id of the user to delete
   * @return Array     the elements status=OK if success, else status=FAIL
   */
  public function deleteUser($id) {
    $sql = 'DELETE FROM user WHERE id=?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    if ($sth->rowCount()==1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL');
    }
  }
}
