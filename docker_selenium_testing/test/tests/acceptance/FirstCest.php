<?php

class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I) {
      $I->amOnPage('/');
      $I->see('This is static text');
      $I->see('This text was created with JavaScript');
      $I->maximizeWindow();
      $I->makeScreenshot();
    }
}
