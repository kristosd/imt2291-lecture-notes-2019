function objEdit (name, obj) {
  var tmp = '';
  if (name!='') {
    tmp += `<h3>${name}</h3>`;
  }
  tmp += '<table>';
  tmp += '<tr><th>Attributt</th><th>Verdi</th></tr>';
  for (var key in obj) {
    if (typeof obj[key]==='function') {
      continue;
    }
    if (obj[key]!=null) {
      tmp += `<tr><td>${key}</td><td><input type="text" data-key="${key}" value="${obj[key]}"></td></tr>`;
    }
  }
  tmp += '</table>';
  return tmp;
}
