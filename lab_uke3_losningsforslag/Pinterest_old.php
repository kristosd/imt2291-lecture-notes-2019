<?php
/**
 * Contains methods to search pinterest for images.
 */
class Pinterest {

  /**
   * Searches pinterest for images matching the given search query.
   *
   * @return an array with urls to images matching the search query.
   */
  public static function getPins ($searchString) {
    $searchString = str_replace (" ", "%20", $searchString);
    $data = file_get_contents("https://no.pinterest.com/search/pins/?q=$searchString");
    //echo $data;
    $dom = new DOMDocument;
    @$dom->loadHTML($data);
    $xpath = new DOMXPath($dom);
    $nlist = $xpath->query("//div[@class='GrowthUnauthPinImage']/a/img");
    $tmp = [];
    foreach($nlist as $img) {
      $srcSet =  $img->getAttribute('srcset');
      $srcSet = explode(", ", $srcSet);
      foreach ($srcSet as $key => $value) {
        $srcSet[$key] = trim(substr($value, 0, strlen($value)-3));
      }
      $tmp[] = $srcSet[0];
    }
    return $tmp;
  }

  /**
   * Searches pinterest for images matching the given search query.
   *
   * @return a two dimensional array with information about the images found.
   *        Each element of the array contains an assosiative array with the following information:
   *        img => the url for the image to show.
   *        url => the relative pinterest url to show the image and related information on Pinterest
   *        text => a string describing the image.
   */
   public static function getPinsWithURLS ($searchString) {
    $searchString = str_replace (" ", "%20", $searchString);
    $data = file_get_contents("https://no.pinterest.com/search/pins/?q=$searchString");
    // echo $data;
    $dom = new DOMDocument;
    @$dom->loadHTML($data);                // Convert string to DOM
    //echo $data;
    $xpath = new DOMXPath($dom);          // Search the DOM with XPath
    $nlist = $xpath->query("//div[@class='GrowthUnauthPinImage']/a"); // Find any a tags within a div tag with given class
    $tmp = [];
    foreach($nlist as $link) {            // Loop over all found a tags
      $url = $link->getAttribute('href'); // Get the href attribute
      $children = $link->childNodes;      // Find all children av the a tag
      foreach ($children as $child) {     // Loop over them
        if ($child->tagName==="img") {    // Is this an img tag
          $srcSet =  $child->getAttribute('srcset');  // Get all sources
          $srcSet = explode(", ", $srcSet);           // Split on ","
          foreach ($srcSet as $key => $value) {       // Loop over all results
            $srcSet[$key] = trim(substr($value, 0, strlen($value)-3));  // Remove everything but the url
          }
          $img = $srcSet[0];              // We just want the first url
        }
      } // We now have the href to pinterest and the src for the image

      // Need to find the text for the image, located in the parents next sibling
      $div = $link->parentNode->nextSibling;
      $h3 = $xpath->query("div/h3", $div);  // In this, find a div with an h3
      $text = "";
      foreach ($h3 as $current) {         // If any text for this image, get it
        $text = $current->textContent;
      }
      // Add the information for this image to the array
      $tmp[] = array ("img" => $img, "url" => $url, "text" => $text);
    }
    return $tmp;
  }
}
