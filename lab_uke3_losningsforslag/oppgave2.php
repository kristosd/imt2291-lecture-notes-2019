<?php
require_once '../twig/vendor/autoload.php';
require_once "Pinterest.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

$pins = Pinterest::getPins("mathematical riddles fun");
$data['search'] = "mathematical riddles fun";
$data['pins'] = $pins;

echo $twig->render('oppgave2.html', $data);
