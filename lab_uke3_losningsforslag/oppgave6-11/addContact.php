<?php

require_once '../../twig/vendor/autoload.php';
require_once "classes/Contacts.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['addContact'])) {
  echo $twig->render('addContactForm.html', array());
} else {
  $data['givenName'] = $_POST['givenName'];
  $data['familyName'] = $_POST['familyName'];
  $data['phone'] = $_POST['phone'];
  $data['email'] = $_POST['email'];

  $contacts = new Contacts();
  $res = $contacts->addContact ($data);
  $res['data'] = $data;

  echo $twig->render('contactAdded.html', $res);
}
