import {qs as $} from './util.js';

export default class Slideshow {
  constructor (id, theme='') {
    this.id = id;
    $(id).innerHTML = '<div></div><div></div>';  // Add two divs into the div
    this.images = [];
    this.currentImage = 1;
    fetch (`bing.php?theme=${theme}`)
    .then(res=>res.json())
    .then(json=> {
      json.value.forEach(img => {
        this.images.push(img.thumbnailUrl);
      });
      $(`${id} div:first-child`).style.backgroundImage = `url(${this.images[0]})`;
    });

    $(`${id} div:last-child`).addEventListener('transitionend', e=>{
      $(`${id} div:last-child`).style.opacity = 0;
      $(`${id} div:first-child`).style.backgroundImage =
                      $(`${id} div:last-child`).style.backgroundImage;
    });

    window.setTimeout(this.slideShow, 5000, this);  // "this" in method slideshow should be "this"
  }

  loadImage(url) {
    return new Promise((resolve, reject)=>{
      const img = document.createElement('img');
      img.onload = resolve;
      img.src = url;
    });
  }

  slideShow(slideshow) {
    slideshow.currentImage++;
    if (slideshow.currentImage==slideshow.images.length) {  // Dersom vi har kommet til slutten så går vi til første bilde igjen
      slideshow.currentImage=0;
    }
    slideshow.loadImage(slideshow.images[slideshow.currentImage]).then (e=>{
      $(`${slideshow.id} div:last-child`).style.backgroundImage = `url(${e.target.src})`;  // Bytt bilde i #img2, vil når det er ferdig lastet bli fadet inn
      $(`${slideshow.id} div:last-child`).style.opacity = 1;
    });
    window.setTimeout(slideshow.slideShow, 5000, slideshow);                      // Kall på nytt om fem sekunder
  }
}
