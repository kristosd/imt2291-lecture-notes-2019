<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

$ch = curl_init();
curl_setopt ($ch, CURLOPT_URL, 'https://data.graphicnews.com/infostradasports/json/olympics/2018/'.$_GET['file']);
curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_exec($ch);

curl_close($ch);
//readfile ('https://data.graphicnews.com/infostradasports/json/olympics/2018/'.$_GET['file']);
