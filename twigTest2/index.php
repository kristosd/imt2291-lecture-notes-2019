<?php
require_once "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

$data = json_decode(file_get_contents('http://folk.ntnu.no/oeivindk/tvguide.php?date=20190117'),true);

// echo "<pre>";
// print_r ($data);

for ($i=0; $i<count($data['channels']); $i++) {
  for ($ii=0; $ii<count($data['channels'][$i]['schedule']); $ii++) {
    $data['channels'][$i]['schedule'][$ii]['startTime'] =
      date('H:i',$data['channels'][$i]['schedule'][$ii]['start']/1000);
  }
}

echo $twig->render('tvguide.html', $data);
