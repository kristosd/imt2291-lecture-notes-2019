<?php
header ("Content-type: application/json");

$bbforksLocation = '/Users/oeivindk/Documents/IMT3281/bbforks';

$repos = [];
//exec("echo ${_POST['pwd']} | $bbforksLocation {$_POST['bbUname']} {$_POST['repo']} 2>&1", $repos);
//exec("echo ${_POST['pwd']} | $bbforksLocation 2>&1", $repos);

$login = $_POST['bbUname'];
$password = $_POST['pwd'];
$url = "https://bitbucket.org/api/2.0/repositories/$login/{$_POST['repo']}/forks?pagelen=100";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
$result = curl_exec($ch);
curl_close($ch);
$jsonData = json_decode($result);

foreach ($jsonData->values as $repo) {
  $repos[count($repos)] = $repo->full_name;
}

//for ($i=0; $i<count($repos); $i++) {
//  list($user, $repo) = explode (',', $repos[$i]);
//  $repos[$i] = $repo;
//}

//array_shift ($repos); // Drop first element (line with "Owner/repo" )

echo json_encode ($repos);
