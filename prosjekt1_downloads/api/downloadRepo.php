<?php
header ('Content-type: application/json');
$repo = $_POST['repo'];
$baseFolder = $_POST['downloadFolder'];
$uname = $_POST['bbUname'];
$pwd = $_POST['pwd'];

chdir ($baseFolder);
list ($folder, $dump) = explode ("/",$repo);
delTree ($folder);
mkdir ($folder);
chdir ($folder);

$tmp = [];
$res = exec("git clone https://$uname:$pwd@bitbucket.org/$repo 2>&1", $tmp);

echo json_encode(array("status"=>"ok", "res"=>$res));

function delTree($dir) {
 $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
}
