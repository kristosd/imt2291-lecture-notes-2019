<?php
require_once '../twig/vendor/autoload.php';

$home = 'http://localhost/imt2291/mod_rewrite';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    // 'cache' => './compilation_cache',
));

if (!isset($_GET['first'])) { // Show main page
  echo $twig->render('index.html', array('source' => 'https://gist.github.com/RaVbaker/2254618',
                                          'home' => $home));
} else if ($_GET['first']==='users') { // Users path selected
  if ($_GET['second']==='') { // No sub page selected
    echo $twig->render('users.html', array ('home' => $home));
  } else if ($_GET['second']==='/add') {
    echo $twig->render('addUser.html', array ('home' => $home));
  } else if ($_GET['second']==='/list') {
    echo $twig->render('listUser.html', array ('home' => $home));
  } else if ($_GET['second']==='/search') {
    echo $twig->render('searchUser.html', array ('home' => $home));
  } else if ($_GET['second']==='/delete') {
    echo $twig->render('deleteUser.html', array ('home' => $home));
  }
}
