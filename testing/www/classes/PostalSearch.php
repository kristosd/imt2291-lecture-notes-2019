<?php

final class PostalSearch {
  private static $geonamesUserName = "okolloen";

  public static function postalCodeSearch($postal) {
    $res = file_get_contents ("http://api.geonames.org/postalCodeSearchJSON?postalcode=".urlencode($postal)."&maxRows=10&country=no&username=".PostalSearch::$geonamesUserName);

    $data = json_decode($res);
    return $data->postalCodes;
  }

  public static function placeNameSearch($postal) {
    $res = file_get_contents ("http://api.geonames.org/postalCodeSearchJSON?placename=".urlencode($postal)."&maxRows=10&country=no&username=".PostalSearch::$geonamesUserName);

    $data = json_decode($res);
    return $data->postalCodes;
  }
}
