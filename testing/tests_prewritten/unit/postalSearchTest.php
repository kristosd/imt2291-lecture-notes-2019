<?php

require_once(__DIR__ . "/../../../html/classes/PostalSearch.php");

class postalSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testPostalCodeSearch() {
      $this->assertEquals('Lena',
                          PostalSearch::postalCodeSearch('2850')[0]->placeName);
    }

    public function testPlaceNameSearch() {
      $postalInfo = PostalSearch::placeNameSearch('Lena');
      $this->assertEquals(2, count($postalInfo));
      $this->assertEquals("2850", $postalInfo[0]->postalCode);
    }
}
