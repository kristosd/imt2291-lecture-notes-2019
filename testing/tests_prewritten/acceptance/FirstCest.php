<?php

class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function frontpageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Postal code or place');
        $I->seeElement('input[value="Search"]');
    }

    public function searchWorks(AcceptanceTester $I) {
      $I->amOnPage('/');
      $I->fillField('postal', 'Lena');
      $I->click('Search');
      $I->see('2850 Lena');
      $I->see('2851 Lena');
    }
}
